<?php

namespace App\Http\Transformer;

use App\Concerns\Formatter;
use League\Fractal\TransformerAbstract;
use App\Models\Company as CompanyModel;

class CompanyTransformer extends TransformerAbstract
{
    use Formatter;

    public function transform(CompanyModel $branches)
    {
         return[
            'id'=> $companies->uuid,
            'code'=> $companies->code,
            'name'=> $companies->name,
            'is_active'=> isBoolean($companies->is_active) ,
            'created_at'=> $companies->created_at->format('c'),
            'updated_at'=> $companies->updated_at->format('c')
        ];
    }
}
