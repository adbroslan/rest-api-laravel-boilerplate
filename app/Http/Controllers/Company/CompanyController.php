<?php

namespace App\Http\Controllers\company;

use App\company;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\Http\Transformers\CompanyTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use App\Processors\Company\Company as CompanyProcessor;

class CompanyController extends Controller
{
    /**
    * Show all Company
    *
    * Get a JSON representation of all the registered Company.
    *
    * @Get("/")
    */
    public function index(CompanyProcessor $processor){
        return $processor->index($this);
    }

    /**
    * Show certain record of Company
    *
    * Get a certain record of JSON representation of certain the registered Company.
    *
    * @Get("/{id_here}")
    */
    public function show($uuid,CompanyProcessor $processor){
        return $processor->show($this,$uuid);
    }

    /**
    * Store a record of Company
    *
    * Store Company Records.
    *
    * @Post("/")
    */
    public function store(CompanyProcessor $processor){
        return $processor->store($this, Input::all());
    }

    /**
    * Update a record of Company
    *
    * Update Selected Company Records.
    *
    * @Put("/{id_here}")
    */
    public function update(CompanyProcessor $processor,$companyUuid)
    {
        return $processor->update($this, $companyUuid, Input::all());
    }

    /**
    * Delete a record of Company
    *
    * Delete selected Company Records.
    *
    * @Delete("/{id_here}")
    */
    public function destroy(CompanyProcessor $processor, $companyUuid)
    {
        return $processor->delete($this, $companyUuid);
    }

    public function showCompanyListing($company)
    {
        return $this->response->paginator($company, new CompanyTransformer);
    }

    public function showCompany($company)
    {
        return $this->response->item($company, new CompanyTransformer);
    }

    public function validationFailed($errors)
    {
        throw new StoreResourceFailedException('Create company failed ,Missing Parameters', $errors);
    }

    public function companyDoesNotExistsError()
    {
        return $this->response->errorNotFound("company does not exists");
    }

}
