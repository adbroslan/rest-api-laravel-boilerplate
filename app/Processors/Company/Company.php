<?php

namespace App\Processors\Company;

use Carbon\Carbon;
use Request;
use App\Processors\Processor;
use App\Models\Company as CompanyModel;

use App\Validators\Company as Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Dingo\Api\Exception\StoreResourceFailedException as StoreFailed;
use Dingo\Api\Exception\DeleteResourceFailedException as DeleteFailed;
use Dingo\Api\Exception\ResourceException as ResourceFailed;
use Dingo\Api\Exception\UpdateResourceFailedException as UpdateFailed;

class CompanyProcessor extends Processor
{
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }


     public function index($listener)
    {
        try {
            $companies =  CompanyModel::paginate(15);
        } catch(ModelNotFoundException $e) {
            return $listener->companiesDoesNotExistsError();
        }
        return $listener->showCompanyListing($companies);
    }

    public function show($listener, $companyUuid)
    {

        try {
            $company = CompanyModel::where('uuid',$companyUuid)->firstorfail();
        } catch(ModelNotFoundException $e) {
            return $listener->companiesDoesNotExistsError();
        }

        return $listener->showCompany($company);
    }

    public function store($listener, array $inputs)
    {
        //use validator when retrieving input
        $validator = $this->validator->on('create')->with($inputs);
        if ($validator->fails()) {
            throw new StoreFailed('Could not create new company.', $validator->errors());
        }


        $company = CompanyModel::create([
            'title' =>  $inputs['title'] ,
            'description' =>  $inputs['description'],
        ]);

        return setApiResponse('success','created','company');
    }

    public function update($listener, $companyUuid, array $inputs)
    {
        //use validator when retrieving input
        $validator = $this->validator->on('update')->with($inputs);
        if ($validator->fails()) {
            throw new UpdateFailed('Could not update company.', $validator->errors());
        }
        try {
            $company = CompanyModel::where('uuid',$companyUuid)->firstorfail();
        } catch(ModelNotFoundException $e) {
            return $listener->companyDoesNotExistsError();
        }


        $update = $company->update([
            'title' =>  $inputs['title'] ,
            'description' =>  $inputs['description'],
            'start' =>  $inputs['start'],
            'end' =>  $inputs['end'],
            'assignor_id' => auth()->user()->id
        ]);

        return setApiResponse('success','updated','company');
    }

    public function delete($listener,$companyUuid)
    {
        if(!$companyUuid){
            throw new DeleteFailed('Could not delete company.');
        }

        try {
            $company = CompanyModel::where('uuid',$companyUuid)->firstorfail();
        } catch(ModelNotFoundException $e) {
            return $listener->companyDoesNotExistsError();
        }
        $company->delete();


        return setApiResponse('success','deleted','company');
    }
}
