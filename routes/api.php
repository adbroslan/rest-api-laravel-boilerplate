<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    //route that doesnt needed token
    $api->group(['namespace' => 'App\Http\Controllers\V1'], function ($api) {
        $api->post('auth/login', 'Auth\AuthController@login');
    });

    //guarded route
    $api->group(['namespace' => 'App\Http\Controllers\V1'], function ($api) {
        $api->post('auth/login', 'Auth\AuthController@login');
        $api->group(['prefix' => 'companies', 'namespace' => 'Company'], function ($api) {
            $api->get('/', 'CompanyController@index');
            $api->get('/{id}', 'CompanyController@show');
            $api->post('/', 'CompanyController@store');
            $api->put('/{id}', 'CompanyController@update');
            $api->delete('/{id}', 'CompanyController@destroy');
        });
    });
});
